# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def home
    render 'home/home'
  end
end
