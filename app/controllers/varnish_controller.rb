# frozen_string_literal: true

class VarnishController < ApplicationController
  def index
    logs = varnish_raw_logs.split("\n").map { |record| record.scan(/".*?"/) }
    @files = top_files(logs)
    @hostnames = top_hostnames(logs)
  end

  private

  def varnish_raw_logs
    File.read(ENV['VARNISH_LOG_PATH'])
  end

  def top_files(logs)
    files = logs.each_with_object(Hash.new(0)) { |e, total| total[e[0]] += 1; }
    files.sort_by { |_key, value| value }.last(5).reverse
  end

  def top_hostnames(logs)
    hostnames = logs.each_with_object(Hash.new(0)) { |e, total| total[hostname_of(e[0])] += 1; }
    hostnames.sort_by { |_key, value| value }.last(5).reverse
  end

  def hostname_of(url)
    url[%r{://.*?/}].gsub(%r{/|:}, '')
  end
end
