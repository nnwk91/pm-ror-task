# frozen_string_literal: true

require 'open-uri'

class JsonController < ApplicationController
  def index
    @items = JSON.parse(parse_json).sort_by { |item| item['price'].to_f }
  end

  private

  def parse_json
    URI.parse(ENV['JSON_URL']).open.read
  end
end
