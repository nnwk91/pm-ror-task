# frozen_string_literal: true

require 'open-uri'

class XmlController < ApplicationController
  def index
    @items = parse_xml.xpath('//item').sort_by { |item| item.at('price').text.to_f }
  end

  private

  def parse_xml
    Nokogiri::XML(URI.parse(ENV['XML_URL']).open).remove_namespaces!
  end
end
