# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'application#home'

  get 'varnish' => 'varnish#index', as: :varnish
  get 'xml' => 'xml#index', as: :xml
  get 'json' => 'json#index', as: :json
end
