# Setup

Create .env.local file and provide path to varnish logs.

```
XML_URL='<url>'
JSON_URL='<url>'
VARNISH_LOG_PATH='<path>'
```

Run `bundle install` and `rails s` to start server.