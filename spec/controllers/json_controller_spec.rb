# frozen_string_literal: true

require 'rails_helper'

RSpec.describe JsonController, type: :request do
  # rubocop:disable Layout/LineLength
  def fake_json
    '[{"access_token":"YZJE","active":true,"asin_code":null,"base_perform_calculation":false,"base_quantity":1,"base_quantity_package":1.0,"base_quantity_sold":1,"base_unit":"Stück","boolean_option_01":false,"bulk_discounts":[],"bulk_discounts_active":false,"calculates_shipping_cost":true,"category":"CATEGORY-name","category_handle":"category-name","code":"dsdfg","compare_at_price":"0.0","considers_stock":false,"content_meta_description":null,"content_meta_keywords":null,"content_title_tag":null,"created_on":"2017-07-07T11:18:37.000Z","custom_url":null,"custom_url_routing":"standard_url_is_canonical","date_option_01":null,"date_option_02":null,"dawanda_id":null,"description":null,"description_html":null,"ebay_code":null,"extra_price":"0.0","featured":false,"featured_image_url":"http://assets.versacommerce.de/images/2bcf4473fa1f361fd636e20e68162c4e72e79594.jpg","format":"tinymce","handle":"datafeedwatch-demo-product","height":0.0,"id":604105,"isbn_code":null,"length":0.0,"minimum_stock":3,"mpn_code":null,"new":null,"note":null,"option_01":"size1","option_01_label":"Size","option_02":"color1","option_02_label":"Color","option_03":null,"option_03_label":null,"packaging_duration":0,"position":null,"price":"4534.0","product_id":null,"product_images_count":1,"properties_count":0,"purchase_price":null,"shop_id":6183,"shows_stock_amount":false,"stock":2,"subtitle":null,"tag_list":null,"tags_count":0,"template_suffix":"product","title":"Datafeedwatch demo product","updated_on":"2017-07-17T12:00:00.000Z","vendor":"vendor -name","vendor_handle":"vendor-name","visible":true,"weight":0.0,"width":0.0,"featured_image_id":693161,"image_tag_list":null,"variants_count":4,"url":"/products/datafeedwatch-demo-product","variant_ids":[604113,607814,611343,611344],"minimum_shipping_cost":0.0,"skip_webhooks":false,"skip_app_webhooks":false,"tax_category":"standard","full_url":"http://datafeedwatch.versacommerce.de/products/datafeedwatch-demo-product"},{"access_token":"NgHS","active":true,"asin_code":null,"base_perform_calculation":false,"base_quantity":1,"base_quantity_package":1.0,"base_quantity_sold":1,"base_unit":"Stück","boolean_option_01":false,"bulk_discounts":[],"bulk_discounts_active":false,"calculates_shipping_cost":true,"category":"unbestimmt","category_handle":"unbestimmt","code":"dsdfg-1","compare_at_price":"0.0","considers_stock":false,"content_meta_description":null,"content_meta_keywords":null,"content_title_tag":null,"created_on":"2017-07-07T11:24:07.000Z","custom_url":null,"custom_url_routing":"standard_url_is_canonical","date_option_01":null,"date_option_02":null,"dawanda_id":null,"description":null,"description_html":null,"ebay_code":null,"extra_price":"0.0","featured":false,"featured_image_url":null,"format":"tinymce","handle":"datafeedwatch-demo-product-1","height":0.0,"id":604113,"isbn_code":null,"length":0.0,"minimum_stock":null,"mpn_code":null,"new":null,"note":null,"option_01":"size2","option_01_label":"Size","option_02":"color2","option_02_label":"Color","option_03":null,"option_03_label":null,"packaging_duration":0,"position":null,"price":"4000.0","product_id":604105,"product_images_count":0,"properties_count":0,"purchase_price":null,"shop_id":6183,"shows_stock_amount":false,"stock":0,"subtitle":null,"tag_list":null,"tags_count":0,"template_suffix":"product","title":"Datafeedwatch demo product 1","updated_on":"2017-07-17T12:00:00.000Z","vendor":"unbestimmt","vendor_handle":"unbestimmt","visible":true,"weight":0.0,"width":0.0,"featured_image_id":null,"image_tag_list":null,"variants_count":0,"url":"/products/datafeedwatch-demo-product-1","variant_ids":[],"minimum_shipping_cost":0.0,"skip_webhooks":false,"skip_app_webhooks":false,"tax_category":"standard","full_url":"http://datafeedwatch.versacommerce.de/products/datafeedwatch-demo-product-1"}]'
  end
  # rubocop:enable Layout/LineLength

  describe '#index' do
    before do
      allow_any_instance_of(described_class).to receive(:parse_json).and_return(fake_json)
    end

    it 'shows json page' do
      get '/json'
      expect(response).to render_template(:index)
    end

    it 'sort by price' do
      get '/json'
      expect(assigns(:items).first['title']).to eq 'Datafeedwatch demo product 1'
    end
  end
end
