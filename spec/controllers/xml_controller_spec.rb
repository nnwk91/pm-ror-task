# frozen_string_literal: true

require 'rails_helper'

RSpec.describe XmlController, type: :request do
  # rubocop:disable Layout/LineLength
  def fake_xml
    xml = Nokogiri::XML('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"><channel><title>BigCommerce Test Shop</title><link>https://dtdstdrgdfgdg.com</link><description/><item><g:id>33</g:id><title>[Sample] Anna, multi-colored bangles Anna New</title><description><strong>How to write product descriptions that sell</strong><br>One of the best things you can do to make your store successful is invest some time in writing great product descriptions. You want to provide detailed yet concise information that will entice potential customers to buy.<br><br>Keep three things in mind:<br><br><strong>Think like a consumer</strong><br>Think about what you as a consumer would want to know, then include those features in your description. For clothes: materials and fit. For food: ingredients and how it was prepared. Bullets are your friends when listing features &mdash; try to limit each one to 5-8 words.<br><br><strong>Find differentiators</strong><br>Pepper your features with details that show how the product stands out against similar offerings. For clothes: is it vintage or hard to find? For art: is the artist well known? For home d&eacute;cor: is it a certain style like mid-century modern? Unique product descriptions not only help you stand out, they improve your SEO.<br><br><strong>Keep it simple</strong><br>Provide enough detail to help consumers make an informed decision, but don&rsquo;t overwhelm with a laundry list of features or flowery language. Densely pack your descriptions with useful information and watch products fly off the shelf.</description><link>https://dtdstdrgdfgdg.com/anna-multi-colored-bangles/?dfw_tracker=67795-33</link><g:image_link>https://cdn2.bigcommerce.com/n-nr1m3w/w40xoh/products/33/images/239/cocolee_anna_92851__19446.1348448816.220.290.jpg?c=2</g:image_link><g:price>50.0 EUR</g:price><g:condition>New</g:condition><g:availability>in stock</g:availability><g:identifier_exists>TRUE</g:identifier_exists><g:brand/><g:color>Red</g:color><g:item_group_id>33</g:item_group_id><g:gtin/><g:google_product_category>1604</g:google_product_category></item><item><g:id>34</g:id><title>[Sample] Harper, tan leather woven belt Harper New</title><description><strong>How to write product descriptions that sell</strong><br>One of the best things you can do to make your store successful is invest some time in writing great product descriptions. You want to provide detailed yet concise information that will entice potential customers to buy.<br><br>Keep three things in mind:<br><br><strong>Think like a consumer</strong><br>Think about what you as a consumer would want to know, then include those features in your description. For clothes: materials and fit. For food: ingredients and how it was prepared. Bullets are your friends when listing features &mdash; try to limit each one to 5-8 words.<br><br><strong>Find differentiators</strong><br>Pepper your features with details that show how the product stands out against similar offerings. For clothes: is it vintage or hard to find? For art: is the artist well known? For home d&eacute;cor: is it a certain style like mid-century modern? Unique product descriptions not only help you stand out, they improve your SEO.<br><br><strong>Keep it simple</strong><br>Provide enough detail to help consumers make an informed decision, but don&rsquo;t overwhelm with a laundry list of features or flowery language. Densely pack your descriptions with useful information and watch products fly off the shelf.</description><link>https://dtdstdrgdfgdg.com/harper-tan-leather-woven-belt/?dfw_tracker=67795-34</link><g:image_link>https://cdn2.bigcommerce.com/n-nr1m3w/w40xoh/products/34/images/250/HERO_GF_sportG_91894__07966.1348465401.220.290.jpg?c=2</g:image_link><g:price>40.0 EUR</g:price><g:condition>New</g:condition><g:availability>in stock</g:availability><g:identifier_exists>TRUE</g:identifier_exists><g:brand/><g:color/><g:item_group_id>34</g:item_group_id><g:gtin/><g:google_product_category>1604</g:google_product_category></item></channel></rss>')
    xml.remove_namespaces!
    xml
  end
  # rubocop:enable Layout/LineLength

  before do
    allow_any_instance_of(described_class).to receive(:parse_xml).and_return(fake_xml)
  end

  describe '#index' do
    it 'shows xml page' do
      get '/xml'
      expect(response).to render_template(:index)
    end

    it 'sort by price' do
      get '/xml'
      expect(assigns(:items).first.at('title').text).to eq '[Sample] Harper, tan leather woven belt Harper New'
    end
  end
end
