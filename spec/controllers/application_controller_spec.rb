# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationController, type: :request do
  describe '#home' do
    it 'shows home page' do
      get '/'
      expect(response).to render_template(:home)
    end
  end
end
