# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VarnishController, type: :request do
  describe '#index' do
    it 'shows varnish page' do
      get '/varnish'
      expect(response).to render_template(:index)
    end

    it 'show top file' do
      get '/varnish'
      expect(assigns(:files).first).to eq ['"GET http://vglive.no/data/events/?le=1337772913 HTTP/1.1"', 20]
    end

    it 'show top hostname' do
      get '/varnish'
      expect(assigns(:hostnames).first).to eq ['www.vg.no', 412]
    end
  end
end
